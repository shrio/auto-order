package com.example.sheref.autoorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.example.sheref.autoorder.Model.CategoryAdapter;
import com.example.sheref.autoorder.Model.CategoryDataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private ExpandableListView expandableListView;
    private List<String> categoryNames;
    private HashMap<String,List<CategoryDataProvider>> listDataChild;
    private List<CategoryDataProvider> categoryDataProviders;
    private CategoryAdapter adapter;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        populateCategory();
    }

    public  void populateCategory(){

        expandableListView = (ExpandableListView) findViewById(R.id.categories);
        categoryNames = new ArrayList<>();
        listDataChild = new HashMap<String,List<CategoryDataProvider>>();
        categoryDataProviders = new ArrayList<CategoryDataProvider>();
        categoryNames.add("Food Categories");

        intent = getIntent();
        String restName = intent.getStringExtra("restName");
        if(restName.equals("Macdonals")){
            categoryDataProviders.add(new CategoryDataProvider("Beef", R.drawable.meat_icon));
            categoryDataProviders.add(new CategoryDataProvider("Chicken", R.drawable.chicken_icon));
            categoryDataProviders.add(new CategoryDataProvider("Fish" , R.drawable.fish_icon));
            categoryDataProviders.add(new CategoryDataProvider("Salads" , R.drawable.salad_icon));
            categoryDataProviders.add(new CategoryDataProvider("Drinks" , R.drawable.drink_icon));
            categoryDataProviders.add(new CategoryDataProvider("Desert" , R.drawable.desert_icon));
            intent = new Intent(getApplicationContext(),MenuActivity.class);
            intent.putExtra("restID","1");
        }

        else if(restName.equals("Papa John's")){
            categoryDataProviders.add(new CategoryDataProvider("Beef", R.drawable.meat_icon));
            categoryDataProviders.add(new CategoryDataProvider("Chicken", R.drawable.chicken_icon));
            categoryDataProviders.add(new CategoryDataProvider("Fish" , R.drawable.fish_icon));
            categoryDataProviders.add(new CategoryDataProvider("Veggie" , R.drawable.veggie_icon));
            categoryDataProviders.add(new CategoryDataProvider("Cheese" , R.drawable.cheese_icon));
            categoryDataProviders.add(new CategoryDataProvider("Drinks" , R.drawable.drink_icon));
            categoryDataProviders.add(new CategoryDataProvider("Desert" , R.drawable.desert_icon));
            intent = new Intent(getApplicationContext(),MenuActivity.class);
            intent.putExtra("restID","2");
        }

        listDataChild.put(categoryNames.get(0), categoryDataProviders);

        adapter = new CategoryAdapter(categoryNames,getApplicationContext(),listDataChild);
        expandableListView.setAdapter(adapter);

        expandableListView.expandGroup(0);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
                CategoryDataProvider provider = listDataChild.get(categoryNames.get(i)).get(i2);
                intent.putExtra("catName",provider.getCatName());
                startActivity(intent);
                return true;
            }
        });
    }
}
