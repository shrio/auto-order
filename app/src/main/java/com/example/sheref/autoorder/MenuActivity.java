package com.example.sheref.autoorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sheref.autoorder.Model.MenuAdapter;
import com.example.sheref.autoorder.Model.MenuDataProvider;

public class MenuActivity extends AppCompatActivity {

    private ListView listView;
    private MenuAdapter adapter;
    private String[] mealNames;
    private String[] mealPrices;
    private String[] mealImages;
    private int[] likeMeals;

    private int cartImage = R.drawable.buy;

    private String[] likesNum;
    private String[] dislikesNum;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        populateMenu();
    }


    public void populateMenu(){
        listView = (ListView) findViewById(R.id.listView);
        adapter = new MenuAdapter(getApplicationContext(),R.layout.menu_row);
        listView.setAdapter(adapter);

        intent = getIntent();
        String restId = intent.getStringExtra("restID");
        String catName = intent.getStringExtra("catName");

        if(restId.equals("1")){
            if(catName.equals("Beef")){

                mealNames = new String[]{"Kofta Burger","Quarter Pounder","Big Mac","Big Tasty"};
                mealPrices = new String[]{"60$" , "85$" , "125$","100$"};
                likesNum = new String[]{"61","90", "55","30"};
                dislikesNum = new String[]{"20","132", "69","25"};
                mealImages = new String[]{"https://i.imgur.com/U1LnCLF.jpg" , "https://i.imgur.com/0gph7tX.jpg",
                                           "https://i.imgur.com/GkpPGEJ.jpg", "https://i.imgur.com/kgurzdM.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,
                        R.drawable.liked,R.drawable.dislike,R.drawable.liked,R.drawable.dislike};
            }

           else if(catName.equals("Chicken")){

                mealNames = new String[]{"Grand Chicken","Grand Spicy","Big Mac","Big Tasty"};
                mealPrices = new String[]{"60$" , "85$" , "125$","100$"};
                likesNum = new String[]{"61","90", "55","30"};
                dislikesNum = new String[]{"20","132", "69","25"};
                mealImages = new String[]{"https://i.imgur.com/0sasH7r.jpg" , "https://i.imgur.com/iojD29i.jpg",
                                           "https://i.imgur.com/Jw51X3v.png","https://i.imgur.com/Z3tpwDy.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,
                        R.drawable.liked,R.drawable.dislike,R.drawable.liked,R.drawable.dislike};
            }


            else if(catName.equals("Fish")){
                mealNames = new String[]{"Filet Fish","Double Filet"};
                mealPrices = new String[]{"60$" , "85$"};
                likesNum = new String[]{"61","90"};
                dislikesNum = new String[]{"20","132"};
                mealImages = new String[]{"https://i.imgur.com/eHcQwtv.png" , "https://i.imgur.com/8XJhmw6.png"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked};

            }

            else if(catName.equals("Salads")){
                mealNames = new String[]{"Chicken Salad","Garden Side"};
                mealPrices = new String[]{"60$" , "85$"};
                likesNum = new String[]{"61","90"};
                dislikesNum = new String[]{"20","132"};
                mealImages = new String[]{"https://i.imgur.com/OftiyDE.jpg", "https://i.imgur.com/9zXj5lx.png"};
                likeMeals = new int[]{R.drawable.like ,R.drawable.disliked,R.drawable.liked, R.drawable.dislike};
            }

            else if(catName.equals("Desert")){
                mealNames = new String[]{"Blueberry sundae","Apple pie","Custard pie","Wafel mcflurry","Oreo mcflurry"};
                mealPrices = new String[]{"60$" , "85$" , "125$","100$","20$"};
                likesNum = new String[]{"61","90", "55","30","74"};
                dislikesNum = new String[]{"20","132", "69","25","52"};
                mealImages = new String[]{"https://i.imgur.com/CKhgrfg.jpg" ,"https://i.imgur.com/wFe0rMt.jpg",
                                 "https://i.imgur.com/kdvRERb.png","https://i.imgur.com/cSr7aNt.jpg","https://i.imgur.com/85U8ks2.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                        R.drawable.dislike,R.drawable.liked,R.drawable.dislike,R.drawable.like,R.drawable.disliked};
            }

            else if(catName.equals("Drinks")){

                mealNames = new String[]{"Vanilla milkshake","Chocoshake","cola" ,"Orange juice"};
                mealPrices = new String[]{"10$" , "7$" , "5$","3$"};
                likesNum = new String[]{"61","90", "55","30"};
                dislikesNum = new String[]{"20","132", "69","25"};
                mealImages = new String []{"https://i.imgur.com/BdkG1vK.jpg" , "https://i.imgur.com/TATqL9B.jpg",
                                     "https://i.imgur.com/OQofgdZ.jpg", "https://i.imgur.com/8NIm3Tz.png"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                        R.drawable.dislike,R.drawable.liked,R.drawable.dislike};
            }

        }


    else if(restId.equals("2")){
            if(catName.equals("Beef")){

                mealNames = new String[]{"Super Pizza","Hot & Spicy","Little Italy","Pepperoni","Smokey Pizza"};
                mealPrices = new String[]{"45$" , "70$" , "85$","40$","70$"};
                likesNum = new String[]{"61","90", "55","30","74"};
                dislikesNum = new String[]{"20","132", "69","25","52"};
                mealImages = new String[]{"https://i.imgur.com/kBQeH1m.jpg" ,"https://i.imgur.com/Zz6HLCj.jpg","https://i.imgur.com/SU45TAq.jpg",
                        "https://i.imgur.com/wEspQ9y.jpg","https://i.imgur.com/RHU6Wqt.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                        R.drawable.dislike,R.drawable.liked,R.drawable.dislike,R.drawable.like,R.drawable.disliked};
            }
            else if(catName.equals("Chicken")){
                mealNames = new String[]{"Chicken BBQ","Cha Cha","Mexican ole" ,"Chicken ranch"};
                mealPrices = new String[]{"45$" , "70$" , "85$","40$"};
                likesNum = new String[]{"61","90", "55","30"};
                dislikesNum = new String[]{"20","132", "69","25"};
                mealImages = new String []{"https://i.imgur.com/eRAayKo.jpg" , "https://i.imgur.com/GttftKG.jpg",
                        "https://i.imgur.com/abe8tQc.jpg", "https://i.imgur.com/MSDKu69.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                        R.drawable.dislike,R.drawable.liked,R.drawable.dislike};
            }

            else if(catName.equals("Fish")){
                mealNames = new String[]{"Tuna pizza","Seafood pizza" ,"Shrimp pizza"};
                mealPrices = new String[]{"60$" , "85$","70$"};
                likesNum = new String[]{"61","90","44"};
                dislikesNum = new String[]{"20","132","30"};
                mealImages = new String[]{"https://i.imgur.com/7eZoQHX.jpg" , "https://i.imgur.com/0rcwGbE.jpg",
                                    "https://i.imgur.com/M7LTba6.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                                      R.drawable.dislike};
            }

            else if(catName.equals("Veggie")){
                mealNames = new String[]{"Garden special"};
                mealPrices = new String[]{"60$"};
                likesNum = new String[]{"61"};
                dislikesNum = new String[]{"20"};
                mealImages = new String[]{"https://i.imgur.com/dCqGEpe.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike};
            }

            else if(catName.equals("Cheese")){
                mealNames = new String[]{"Margarita pizza","6-cheese pizza"};
                mealPrices = new String[]{"60$" , "85$"};
                likesNum = new String[]{"61","90"};
                dislikesNum = new String[]{"20","132"};
                mealImages = new String[]{"https://i.imgur.com/lF0CMVK.jpg" , "https://i.imgur.com/2QrERs0.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked};
            }

            else if(catName.equals("Desert")){
                mealNames = new String[]{"Nutella pie"};
                mealPrices = new String[]{"35$"};
                likesNum = new String[]{"61"};
                dislikesNum = new String[]{"20"};
                mealImages = new String[]{"https://i.imgur.com/7W3ybQ0.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike};
            }

            else if(catName.equals("Drinks")){
                mealNames = new String[]{"Cola","Sprite","Mirinda" ,"Minreal water"};
                mealPrices = new String[]{"10$" , "8$" , "5$","3$"};
                likesNum = new String[]{"61","90", "55","30"};
                dislikesNum = new String[]{"20","132", "69","25"};
                mealImages = new String []{"https://i.imgur.com/OQofgdZ.jpg" , "https://i.imgur.com/WXj5Ema.png",
                        "https://i.imgur.com/VdRoBBa.jpg", "https://i.imgur.com/zSIIsP4.jpg"};
                likeMeals = new int[]{R.drawable.liked,R.drawable.dislike,R.drawable.like, R.drawable.disliked,R.drawable.liked,
                        R.drawable.dislike,R.drawable.liked,R.drawable.dislike};
            }
        }

        for(int i=0,j=0;i<mealNames.length;i++,j+=2){

            MenuDataProvider dataProvider = new MenuDataProvider(mealNames[i], mealPrices[i],
                    likesNum[i],dislikesNum[i], mealImages[i],likeMeals[j], likeMeals[j+1],cartImage);

            adapter.add(dataProvider);

        }

    }
}
