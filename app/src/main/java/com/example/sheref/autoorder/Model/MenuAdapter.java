package com.example.sheref.autoorder.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sheref.autoorder.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sheref on 07/01/2018.
 */
public class MenuAdapter extends ArrayAdapter {
    private Context context;
    private List<MenuDataProvider> data = new ArrayList<>();
    private LayoutInflater inflater;

    public MenuAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public class DataHandler{
        private ImageView mealImage, cartImage, likeImage, dislikeImage;
        private TextView mealName,mealPrice, likesNum, dislikesNum;

    }

    @Override
    public void add(Object object) {
        super.add(object);
        data.add((MenuDataProvider) object);
    }
    @Override
    public Object getItem(int position) {
        return this.data.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        final DataHandler handler;
        if(row == null){
            row = inflater.inflate(R.layout.menu_row,parent,false);
            handler = new DataHandler();
            handler.mealImage = (ImageView) row.findViewById(R.id.mealImage);
            handler.cartImage = (ImageView) row.findViewById(R.id.cartImage);
            handler.likeImage = (ImageView) row.findViewById(R.id.like);
            handler.dislikeImage = (ImageView) row.findViewById(R.id.dislike);
            handler.mealName = (TextView) row.findViewById(R.id.mealName);
            handler.mealPrice = (TextView) row.findViewById(R.id.mealPrice);
            handler.likesNum = (TextView) row.findViewById(R.id.likesNum);
            handler.dislikesNum = (TextView) row.findViewById(R.id.dislikesNum);

            row.setTag(handler);
        }

        else{
            handler = (DataHandler) row.getTag();
        }

        MenuDataProvider provider;
        provider = (MenuDataProvider) this.getItem(position);
        Picasso.with(context).load(provider.getMealImage()).into(handler.mealImage);
        handler.mealName.setText(provider.getMealName());
        handler.mealPrice.setText(provider.getMealPrice());
        handler.cartImage.setImageResource(provider.getCartImage());
        handler.likeImage.setImageResource(provider.getLikeImage());
        handler.dislikeImage.setImageResource(provider.getDislikeImage());
        handler.likesNum.setText(provider.getLikesNum());
        handler.dislikesNum.setText(provider.getDislikesNum());

        handler.cartImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(handler.cartImage.getTag().equals("1")){
                    handler.cartImage.setTag("2");
                    handler.cartImage.setImageResource(R.drawable.buying);
                }

                else if(handler.cartImage.getTag().equals("2")){
                    handler.cartImage.setTag("1");
                    handler.cartImage.setImageResource(R.drawable.buy);
                }
            }
        });

        return row;

    }

}
