package com.example.sheref.autoorder.Model;

/**
 * Created by sheref on 07/01/2018.
 */
public class MenuDataProvider {
    private String mealName, mealImage;
    private String mealPrice;
    private String likesNum,dislikesNum;
    private int likeImage, dislikeImage, cartImage;

    public MenuDataProvider(String mealName, String mealPrice, String likesNum, String dislikesNum, String mealImage, int likeImage, int dislikeImage, int cartImage) {
        this.mealName = mealName;
        this.mealPrice = mealPrice;
        this.likesNum = likesNum;
        this.dislikesNum = dislikesNum;
        this.mealImage = mealImage;
        this.likeImage = likeImage;
        this.dislikeImage = dislikeImage;
        this.cartImage = cartImage;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getMealPrice() {
        return mealPrice;
    }

    public void setMealPrice(String mealPrice) {
        this.mealPrice = mealPrice;
    }

    public String getLikesNum() {
        return likesNum;
    }

    public void setLikesNum(String likesNum) {
        this.likesNum = likesNum;
    }

    public String getDislikesNum() {
        return dislikesNum;
    }

    public void setDislikesNum(String dislikesNum) {
        this.dislikesNum = dislikesNum;
    }

    public String getMealImage() {
        return mealImage;
    }

    public void setMealImage(String mealImage) {
        this.mealImage = mealImage;
    }

    public int getLikeImage() {
        return likeImage;
    }

    public void setLikeImage(int likeImage) {
        this.likeImage = likeImage;
    }

    public int getDislikeImage() {
        return dislikeImage;
    }

    public void setDislikeImage(int dislikeImage) {
        this.dislikeImage = dislikeImage;
    }

    public int getCartImage() {
        return cartImage;
    }

    public void setCartImage(int cartImage) {
        this.cartImage = cartImage;
    }
}
