package com.example.sheref.autoorder.Model;

/**
 * Created by sheref on 07/01/2018.
 */
public class CategoryDataProvider {

    private String catName;
    private int catImage;

    public CategoryDataProvider(String catName, int catImage) {
        this.catName = catName;
        this.catImage = catImage;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatImage() {
        return catImage;
    }

    public void setCatImage(int catImage) {
        this.catImage = catImage;
    }
}
