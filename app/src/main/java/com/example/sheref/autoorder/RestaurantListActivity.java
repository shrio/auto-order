package com.example.sheref.autoorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.sheref.autoorder.Model.RestaurantDataProvider;
import com.example.sheref.autoorder.Model.RestaurantListAdapter;

import java.util.ArrayList;
import java.util.List;

public class RestaurantListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RestaurantListAdapter adapter;
    private Intent intent;
    private List<RestaurantDataProvider> data = new ArrayList<>();
    private int []restImages = new int[]{R.drawable.mac_logo, R.drawable.papa_logo};
    private String [] restNames = new String[]{"Macdonals", "Papa John's"};
    private float[] restRatingBar = new float[]{3.5f,4.5f};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);
        populateRestaurants();
    }

    public void populateRestaurants() {
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new RestaurantListAdapter(data,getApplicationContext());
        recyclerView.setLayoutManager( new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        int i = 0;
        for (String name : restNames) {
            RestaurantDataProvider provider = new RestaurantDataProvider(name, restImages[i], restRatingBar[i]);
            data.add(provider);
            ++i;
        }

        adapter.notifyDataSetChanged();
    }
}
