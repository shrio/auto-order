package com.example.sheref.autoorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private Button signInBtn;
    private EditText userName, password;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        signInBtn = (Button) findViewById(R.id.signInBtn);
        userName = (EditText) findViewById(R.id.userNameEditTxt);
        password = (EditText) findViewById(R.id.passEditTxt);

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userName.getText().equals("") || password.getText().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Invalid Input", Toast.LENGTH_LONG).show();
                }
                else
                {
                    intent = new Intent(getApplicationContext(),RestaurantListActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

}
