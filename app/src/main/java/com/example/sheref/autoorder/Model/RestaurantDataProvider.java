package com.example.sheref.autoorder.Model;

/**
 * Created by sheref on 07/01/2018.
 */
public class RestaurantDataProvider {
    private String restName;
    private int restImage;
    private float restRatingBar;

    public RestaurantDataProvider(String restName, int restImage, float restRatingBar) {
        this.restName = restName;
        this.restImage = restImage;
        this.restRatingBar = restRatingBar;
    }

    public String getRestName() {
        return restName;
    }

    public void setRestName(String restName) {
        this.restName = restName;
    }

    public int getRestImage() {
        return restImage;
    }

    public void setRestImage(int restImage) {
        this.restImage = restImage;
    }

    public float getRestRatingBar() {
        return restRatingBar;
    }

    public void setRestRatingBar(float restRatingBar) {
        this.restRatingBar = restRatingBar;
    }
}
